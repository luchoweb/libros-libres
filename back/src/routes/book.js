const router = require('express').Router();

const bookController = require('../controllers/bookController');

router.get('/', bookController.list);
router.post('/add', bookController.save);
router.get('/update/:id', bookController.edit);
router.post('/update/:id', bookController.update);
router.get('/delete/:id', bookController.delete);

module.exports = router;

